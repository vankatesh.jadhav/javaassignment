package assignments;

import java.util.Arrays;

public class Two_Dimenstional_Array {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 int[][] StudentMarks = new int[4][4];
	        
	        StudentMarks[0][0] = 45;  // a
	        StudentMarks[0][1] = 34;  // b
	        StudentMarks[0][2] = 28;  // c
	        
	        StudentMarks[1][0] = 38;  // a
	        StudentMarks[1][1] = 14;  // b
	        StudentMarks[1][2] = 45;  // c
	        
	        StudentMarks[2][0] = 13;  // a
	        StudentMarks[2][1] = 50;  // b
	        StudentMarks[2][2] = 43;  // c
	        
	        StudentMarks[3][0] = 11;  // a
	        StudentMarks[3][1] = 23;  // b
	        StudentMarks[3][2] = 44;  // c
	        
	       
	        System.out.println("Student Marks Matrix");
	        System.out.println(Arrays.deepToString(StudentMarks));

	}

}
